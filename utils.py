
import subprocess
import pyttsx3
import json
import os
from progress.bar import IncrementalBar
from voicers import *
from pydub import AudioSegment
import wave
import datetime
from sex_recognize import *



def delete_from_temp_dir(dir):
    for filename in os.listdir(dir):
        file_path = os.path.join(dir, filename)
        try:
            if os.path.isfile(file_path):
                os.remove(file_path)
        except Exception as e:
            print(f'Ошибка при удалении файла {file_path}. {e}')

def RunConvertor(input_srt,output_json):
    subprocess.call(['SRTtoJSON.exe','-st'+input_srt,"-o"+output_json,"-ntt","-[]"])

def ExtractSoundtrack(input_video,output_soundtrack):
    print('Отделяем звуковую дорожку')
    subprocess.call(['ffmpeg','-i',str(input_video),'-y', '-vn','-ar' ,'48000','-ac','2','-ab','192K','-f','wav',str(output_soundtrack)])


def GetWavLen(json):
    for i in range(0,len(json)):
        if os.path.isfile(json[i]["WAV"]):
            with wave.open(json[i]["WAV"], 'rb') as wav_file:
                sample_rate = wav_file.getframerate()  # Частота дискретизации
                num_frames = wav_file.getnframes()  # Количество фреймов
                # Вычисляем длительность аудиофайла в миллисекундах
                duration_ms = (num_frames / sample_rate) * 1000
            json[i]["len"]=round(duration_ms)
        else:
            json[i]["len"]=0


def CorrectingMontageTime(json):
    for i in range(1,len(json)):
        end_last_wav=int(json[i-1]["len"])+int(json[i-1]["StartTimeMs"])
        starttime=int(json[i]["StartTimeMs"])

        if (starttime<end_last_wav) or (starttime==end_last_wav):
            json[i]["StartTimeMs"]=str(end_last_wav)
        else:
            json[i]["StartTimeMs"]=str(starttime)


def MontageSounds(json,temp_dir,silence):
    print("Загрузка основной звуковой дорожки")
    bar = IncrementalBar('Монтирую фразу ', max = len(json))
    base_track = AudioSegment.from_file(temp_dir+"\\sourse_soundtrack.wav",format="wav")   

    for i in range(0,len(json)):
        if os.path.isfile(json[i]["WAV"]):
            curr_file=os.path.normpath(json[i]["WAV"])
            add_track = AudioSegment.from_wav(curr_file)
            try:
                    base_track = base_track.overlay(add_track, position=int(json[i]["StartTimeMs"]),gain_during_overlay=int(silence))
            except:
                print('Ошибка монтирования файла : ',curr_file, ' INDEX : ',i)
        bar.next()

    base_track.export(temp_dir+"\\voiced_soundtrack.wav", format="wav")
    bar.finish()


def NormalizeSound(old_sound,new_sound,normalized_dBFS):
    normalized_dBFS = float(normalized_dBFS)
    print("Нормализация звука")
    print("Загрузка звуковой дорожки")
    sound = AudioSegment.from_file(old_sound, "wav")
    print('Текущая dBFS :', round(sound.dBFS, 1))
    print('Нормализуем до ', normalized_dBFS)
    dBFS = normalized_dBFS - sound.dBFS
    sound.apply_gain(dBFS)
    print("Сохранение нормализованной звуковой дорожки")    
    sound.export(new_sound, format="wav")


def Speeding(json,speeding):
    for i in range(0,len(json)):
        if os.path.isfile(json[i]["WAV"]):
            os.system('title Ускорение озвученных фраз '+str(i)+' / '+str(len(json)))
            subprocess.call(['ffmpeg', '-i' ,json[i]["WAV"],'-y', '-af', 'atempo='+str(speeding), '-f' ,'wav', json[i]["WAV"]+"_speeding"])
            os.remove(json[i]["WAV"])
            os.rename(json[i]["WAV"]+'_speeding',json[i]["WAV"])
    

def FinalMontage(input_video,out_video,new_soundtrack):
    subprocess.call(['ffmpeg','-i',str(new_soundtrack),'-i',str(input_video),'-y','-c:v','copy',str(out_video)])


def GetRecognizeWav(json,temp_dir):
    for i in range(0,len(json)):
        
        date_time_obj_start = datetime.datetime.strptime(json[i]["StartTime"][:-4], '%H:%M:%S')
        date_time_obj_end = datetime.datetime.strptime(json[i]["EndTime"][:-4], '%H:%M:%S')
        duration=date_time_obj_end-date_time_obj_start
        # получение фрагмента звуковой дорожки
        subprocess.call(["ffmpeg" ,"-ss" ,str(date_time_obj_start)[11:],  "-i", temp_dir+"\\sourse_soundtrack.wav", "-t" ,str(duration) , "-acodec" , "copy", "-ar", "44100", temp_dir+"\\temp_recognize.wav","-y"])
        # удаление из него ненужных частот
        subprocess.call(['ffmpeg','-i',temp_dir+"\\temp_recognize.wav",'-af',"lowpass=780,highpass=85",temp_dir+"\\temp_recognize_only_voice.wav",'-y'])


        tensor,voice=Recognize(temp_dir+"\\temp_recognize_only_voice.wav")
        json[i]["voice"]=voice

        os.system("title Распознавание голоса М-Ж "+str(i)+" / "+str(len(json))+" "+voice+"  "+str(tensor))






def Start(temp_dir,input_srt,input_video,out_video,engine,silence,speeding,default_voice,del_temp_dir,lang,sound_normalization):

    if lang=="":
        lang="rus"

    lang=lang.lower()

    if ((os.path.exists(temp_dir)==False) and (os.path.isfile(temp_dir)==False)):
        print('Создание папки для временных файлов')
        os.mkdir(temp_dir)
    else:
        print('Очистка папки для временных файлов')
        delete_from_temp_dir(temp_dir)

    os.system("title Парсинг субтитров")
    RunConvertor(input_srt,temp_dir+"\\text.json")

    os.system("title Отделение звуковой дорожки")
    ExtractSoundtrack(input_video,temp_dir+"\\sourse_soundtrack.wav")

    data = json.load((open(temp_dir+"\\text.json", 'r',encoding="utf-8")))

    with open(temp_dir+"\\debug.json", "w") as json_file:
        json.dump(data, json_file)

    if default_voice=="AUTO" or default_voice=="AUTO_INVERSION":
        GetRecognizeWav(data,temp_dir)

        if default_voice=="AUTO_INVERSION":
            for i in range(0,len(data)):
                data[i]["voice"]=data[i]["voice"]+"_INVERSION"

    else:
        for i in range(0,len(data)):
            data[i]["voice"]=default_voice


    with open(temp_dir+"\\debug.json", "w") as json_file:
        json.dump(data, json_file)

    if engine=="RHV":
        os.system("title Озвучка движком RHVoice")
        RHVoicer(data,temp_dir,lang)

    if engine=="SILERO":
        os.system("title Озвучка движком Silero")
        SileroVoicer(data,temp_dir,lang)

    if engine=="GTTS":
        os.system("title Озвучка движком Google TTS")
        GTTS(data,temp_dir,lang)


    if speeding!="1.0" and speeding!="1" and speeding!="0" and speeding!=""  and speeding!=1:
        print('Ускорение озвученных фраз')
        Speeding(data,speeding)

    with open(temp_dir+"\\debug.json", "w") as json_file:
        json.dump(data, json_file)


    os.system("title Определение длины WAV")
    print("Определение длины WAV")
    GetWavLen(data)

    with open(temp_dir+"\\debug.json", "w") as json_file:
        json.dump(data, json_file)

    os.system("title Коректировка времени монтажа")
    print("Коректировка времени монтажа")
    CorrectingMontageTime(data)

    with open(temp_dir+"\\debug.json", "w") as json_file:
        json.dump(data, json_file)

    os.system("title Монтирование новой звуковой дорожки")
    MontageSounds(data,temp_dir,silence)

    if sound_normalization!=0:
        NormalizeSound(temp_dir+"\\voiced_soundtrack.wav",temp_dir+"\\voiced_soundtrack.wav",sound_normalization)

    os.system('title Приклеиваем звуковую дорожку к видео')
    print('Финальный монтаж (приклеиваем звуковую дорожку к видео)')
    FinalMontage(input_video,out_video,temp_dir+"\\voiced_soundtrack.wav")

    if del_temp_dir==True:
        print('Удаление временных файлов')
        delete_from_temp_dir(temp_dir)       
        os.rmdir(temp_dir)

    print("Монтаж завершён. Ещё раз ?")
    os.system("title Монтаж завершён. Ещё раз ?")





