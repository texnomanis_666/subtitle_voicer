import torch
import torch.nn as nn


class VoiceSexReconizeNN(nn.Module):
    def __init__(self, n_input_features):

        super().__init__()
        self.activation=nn.Sigmoid()
        self.linear1 = nn.Linear(n_input_features, 128,True)
        self.linear2 = nn.Linear(128, 128,True)
        self.linear3 = nn.Linear(128, 64,True)
        self.linear4 = nn.Linear(64, 32,True)        
        self.linear5 = nn.Linear(32, 1,True)    


    def forward(self, x):
        out = self.activation(self.linear1(x))
        out = self.activation(self.linear2(out))
        out = self.activation(self.linear3(out))
        out = self.activation(self.linear4(out))
        out = self.activation(self.linear5(out))
        return out

