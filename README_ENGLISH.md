﻿## ENGLISH

## Subtitles Voicer v2.4

## Discription

The program is designed for dubbing subtitles and automatically editing the dubbed audio track to the video.

At the entrance to the program, an original video file is submitted, in which there is no Russian voice acting, and subtitles in Russian. The program indicates where to put the output video

file. Using TTS (Text-To-Speech) technology, the program will voice subtitles, edit a new audio track, and edit it to the video. The output is a film with Russian voice acting.

Note. Voice acting is possible in any languages supported by the selected TTS engine, but the current version is intended only for Russian.

## Components and libraries used:

*   TTS Silero [https://pypi.org/project/silero/](https://pypi.org/project/silero/)
*   TTS RHVoice [https://rhvoice.ru/](https://rhvoice.ru/)
*   ffmpeg codec
*   SRT to JSON - converter of subtitles to JSON, my development
*   pydub - library for audio editing, uses ffmpeg codec
*   google-tts - online tts from Google
*   pyttsx3 - library for working with SAPI, allows you to use RHVoice
*   progress - display the progress bar in the console
*   torch - machine learning library, required for Silero to work
*   PyQt5 - graphical interface (GUI)
*   requests - library for working with the http protocol

## Instructions:

1.  Select a video.
2.  Select subtitles if they are not selected automatically.
3.  Specify the output file name. It is automatically generated when you select a source file
4.  Specify the voice engine. Since version 2.4 Silero is the default
5.  Specify service parameters.
6.  Start the process.

When the program is running, a console window will be visible. It is designed to monitor the process and display errors that occur. Do not close it. This is the main program window.

Extra options:  
Fading.  
When editing a new voiceover, it is superimposed on top of the original one. At this point, the original voiceover is made quieter so as not to interfere with hearing the new voiceover. This option indicates how much to make the original sound quieter. Default -15 (minus 15)

Acceleration.  
Each voiced phrase of the new voice acting can be speeded up or slowed down. Default is 1.0.  
Values 1.0, 1, 0 mean do not change the speed.  
2.0 speed up 2 times.  
0.5 slow down by 2 times.

### **ATTENTION ! ! !**

For RHVoice to work correctly, follow these instructions  
[https://rhvoice.ru/installation/](https://rhvoice.ru/installation/)  
When you use it for the first time, Silero will download the neural network model to your computer. Each language has its own model file. All of them are downloaded to the silero_models directory

There is no guarantee that the program will work!

Google TTS supports only one voice for most languages. It makes no sense to use a neural network designed for voice recognition. For Russian, only a female voice is supported.

## Versoin history

Changes in version 2.1

*   Slightly changed the graphical interface
*   Added voice recognition (male or female) for two-voice voice acting. (The neural network sometimes works incorrectly and makes errors; a more extensive dataset is needed to train it)
*   Fixed TTS Google bugs

Changes in version 2.2  
Added:

*   Before work, the folder with temporary files is cleared
*   Ability to delete temporary files after work
*   Ability to launch a program from the console
*   Dark theme

Fix:

*   Error when selecting a temporary folder

Changes in version 2.3  
Added:

*   icon
*   The male/female voice recognizer now has an inverted mode. A man's voice is considered a woman's voice, and a woman's voice is considered a man's voice.

Changed:

*   Interface slightly changed
*   Improved neuronet performance for male/female voice recognition. It is still an experimental feature and is not stable.

Changes in version 2.4
Added:

* Voice acting in different languages ​(not all voices and engines are supported yet)
* Sound normalization

Changed:

* Interface slightly changed

Fixed:

* Silero voice acting dropped out in error in some cases

## Command Line Options

\-st (SubTitles) input srt subtitles file  
Input file with subtitles in srt format

\-o output video  
The name of the output video file. If such a file already exists, it will be overwritten.

*   i input video  
    Input video file.

\-e engine  
TTS engine. Default Silero
Options:  
RHV - RHVoice  
SILERO - Silero  
GTTS - online TTS from Google

\-z silence  
When adding new voice acting, the original audio track is made quieter in those places where the new sound is added. This is done so that the new voice acting can be heard better. This parameter specifies how much to mute the original sound. Default -15

\-td temp dir  
Specify a temporary folder. Default temp\_dir

\-u speeding  
If the resulting phrases in the new voiceover turn out to be too slow, you can speed them up. Default is 1.0 (do not change).

\-dv default voice  
What kind of voice should I use? The default is male for any engine.  
Options:  
AUTO - determine the voice using a neural network (male or female)  
AUTO\_INVERSION - determine the voice using a neural network (male or female) and change the value to the opposite  
M - male  
F - female

Some voice engines support multiple voices; to cycle through the voices, add a number to the letter. For example:  
M1, F2, F3  
Important:  
value M1 == M == first available male voice  
value F1 == F == first available female voice

\-d delete temp dir  
Delete temporary files after work.

\-l Language
Language. Specify the language to voice. The default is Russian.

\-n Normalization
Sound normalization. Default is -10