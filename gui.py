

from utils import Start
from PyQt5.QtWidgets import QLabel, QWidget, QComboBox, QCheckBox, QHBoxLayout, QVBoxLayout, QLineEdit, QPushButton, QFileDialog
from PyQt5.QtGui import QIcon
import os


class SelectVoice(QHBoxLayout):
    def __init__(self):
        super().__init__()

        self.Selected = "M"

        self.radio_button_voice_m = QCheckBox('Мужской')
        self.radio_button_voice_f = QCheckBox('Женский')
        self.radio_button_voice_auto = QCheckBox('Автоопределение нейросетью')
        self.radio_button_voice_auto_inversion = QCheckBox(
            'Hейросеть - инверсия')

        self.addWidget(self.radio_button_voice_m)
        self.addWidget(self.radio_button_voice_f)
        self.addWidget(self.radio_button_voice_auto)
        self.addWidget(self.radio_button_voice_auto_inversion)

        self.radio_button_voice_m.clicked.connect(
            self.radio_button_voice_m_click)
        self.radio_button_voice_f.clicked.connect(
            self.radio_button_voice_f_click)
        self.radio_button_voice_auto.clicked.connect(
            self.radio_button_voice_auto_click)
        self.radio_button_voice_auto_inversion.clicked.connect(
            self.radio_button_voice_auto_inversion_click)

        self.radio_button_voice_m.setChecked(True)

    def radio_button_voice_m_click(self):
        self.Selected = "M"
        self.radio_button_voice_f.setChecked(False)
        self.radio_button_voice_auto.setChecked(False)
        self.radio_button_voice_auto_inversion.setChecked(False)

    def radio_button_voice_f_click(self):
        self.Selected = "F"
        self.radio_button_voice_m.setChecked(False)
        self.radio_button_voice_auto.setChecked(False)
        self.radio_button_voice_auto_inversion.setChecked(False)

    def radio_button_voice_auto_click(self):
        self.Selected = "AUTO"
        self.radio_button_voice_m.setChecked(False)
        self.radio_button_voice_f.setChecked(False)
        self.radio_button_voice_auto_inversion.setChecked(False)

    def radio_button_voice_auto_inversion_click(self):
        self.Selected = "AUTO_INVERSION"
        self.radio_button_voice_auto.setChecked(False)
        self.radio_button_voice_m.setChecked(False)
        self.radio_button_voice_f.setChecked(False)


class SelectEngine(QHBoxLayout):
    def __init__(self):
        super().__init__()

        self.Selected = "SILERO"

        self.radio_button_Silero = QCheckBox('Silero')
        self.radio_button_RHVoice = QCheckBox('RHVoice')
        self.radio_button_gtts = QCheckBox('Google TTS (нужен интернет)')

        self.addWidget(self.radio_button_Silero)
        self.addWidget(self.radio_button_RHVoice)
        self.addWidget(self.radio_button_gtts)

        self.radio_button_Silero.clicked.connect(
            self.radio_button_Silero_m_click)
        self.radio_button_RHVoice.clicked.connect(
            self.radio_button_RHVoice_click)
        self.radio_button_gtts.clicked.connect(self.radio_button_gtts_click)

        self.radio_button_Silero.setChecked(True)

    def radio_button_Silero_m_click(self):
        self.Selected = "SILERO"
        self.radio_button_RHVoice.setChecked(False)
        self.radio_button_gtts.setChecked(False)

    def radio_button_RHVoice_click(self):
        self.Selected = "RHV"
        self.radio_button_gtts.setChecked(False)
        self.radio_button_Silero.setChecked(False)

    def radio_button_gtts_click(self):
        self.Selected = "GTTS"
        self.radio_button_RHVoice.setChecked(False)
        self.radio_button_Silero.setChecked(False)


class SoundNormalization(QHBoxLayout):
    def __init__(self):
        super().__init__()

        self.Value = -10

        label_normalization = QLabel(
            'Нормализация звука в dBFS  0 - выключена')
        self.addWidget(label_normalization)

        self.field_normalization = QLineEdit()
        self.addWidget(self.field_normalization)
        self.field_normalization.setText("-10")

        self.field_normalization.textChanged.connect(self.changeValue)

    def changeValue(self):
        if self.field_normalization.text() != "":
            self.Value = self.field_normalization.text()
        else:
            self.Value = 0


class SelectLanguage(QHBoxLayout):
    def __init__(self):
        super().__init__()

        self.Value = "russian"

        label_select_lang = QLabel('На какой язык озвучивать ?')
        self.addWidget(label_select_lang)

        self.lang_combo_box = QComboBox()
        self.lang_combo_box.addItem('Русский')
        self.lang_combo_box.addItem('English')
        self.lang_combo_box.addItem('Deutsch')

        # эти пока не поддерживаются
        # elf.lang_combo_box.addItem('Espanol')
        # elf.lang_combo_box.addItem('Украинский')
        # elf.lang_combo_box.addItem('French')
        # elf.lang_combo_box.addItem('Татарский')
        # elf.lang_combo_box.addItem('Узбекский')

        self.lang_combo_box.currentIndexChanged.connect(self.changeValue)
        self.addWidget(self.lang_combo_box)

    def changeValue(self):
        if self.lang_combo_box.currentText() != "":
            self.Value = self.lang_combo_box.currentText()
        else:
            self.Value = "russian"


class Speeding(QHBoxLayout):
    def __init__(self):
        super().__init__()

        self.Value = "1.0"

        label_speeding = QLabel(
            'На сколько ускорить голос в новой озвучке. 1.0 не менять')
        self.addWidget(label_speeding)

        self.field_speeding = QLineEdit()
        self.addWidget(self.field_speeding)
        self.field_speeding.setText("1.0")

        self.field_speeding.textChanged.connect(self.changeValue)

    def changeValue(self):
        if self.field_speeding.text() != "":
            self.Value = self.field_speeding.text()

            if float(self.field_speeding.text()) > 2:
                self.Value = "2.0"
                self.field_speeding.setText("2.0")

            if float(self.field_speeding.text()) < 0.5:
                self.Value = "0.5"
                self.field_speeding.setText("0.5")
        else:
            self.Value = "1.0"


class TempDirSelect(QHBoxLayout):
    def __init__(self):
        super().__init__()

        self.Value = "temp_dir"

        self.field_tempdir = QLineEdit()
        self.field_tempdir.setText("temp_dir")
        self.addWidget(self.field_tempdir)

        self.btn_select_tempdir = QPushButton(
            "Выбрать папку для временных файлов")
        self.btn_select_tempdir.clicked.connect(self.btn_select_tempdir_click)
        self.addWidget(self.btn_select_tempdir)

    def btn_select_tempdir_click(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.DirectoryOnly)
        dialog.setOption(QFileDialog.ShowDirsOnly, True)

        if dialog.exec_() == QFileDialog.Accepted:
            selected_folder = dialog.selectedFiles()[0]
            self.field_tempdir.setText(selected_folder)
            self.Value = selected_folder


class GUI(QWidget):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Озвучка субтитров v2.4 by TexnomaniS")
        self.setWindowIcon(QIcon("icon.ico"))
        self.lang = "russian"

        layout = QVBoxLayout()

        label_filmname = QLabel('Исходный видеофайл')
        layout.addWidget(label_filmname)

        self.field_filmname = QLineEdit()
        layout.addWidget(self.field_filmname)

        self.btn_select_film = QPushButton("Выбрать фильм")
        self.btn_select_film.clicked.connect(self.btn_select_film_click)
        layout.addWidget(self.btn_select_film)

        label_srtname = QLabel('Файл с субтитрами')
        layout.addWidget(label_srtname)

        self.field_srtname = QLineEdit()
        layout.addWidget(self.field_srtname)

        self.btn_select_srt = QPushButton("Выбрать субтитры")
        self.btn_select_srt.clicked.connect(self.btn_select_srt_click)
        layout.addWidget(self.btn_select_srt)

        label_outfilm = QLabel('Выходной видеофайл')
        layout.addWidget(label_outfilm)

        self.field_outfilm = QLineEdit()
        layout.addWidget(self.field_outfilm)

        self.btn_select_outfilm = QPushButton("Куда положить выходной файл ?")
        self.btn_select_outfilm.clicked.connect(self.btn_select_outfilm_click)
        layout.addWidget(self.btn_select_outfilm)

        label_silence = QLabel(
            'Затихание (на сколько глушить основную дорожку во время наложения новой озвучки)')
        layout.addWidget(label_silence)

        self.field_silence = QLineEdit()
        layout.addWidget(self.field_silence)
        self.field_silence.setText("-15")

        # поле нормализации
        self.sound_normalization = SoundNormalization()
        layout.addLayout(self.sound_normalization)

        # поле ускорения звука
        self.speeding = Speeding()
        layout.addLayout(self.speeding)

        # поле выбра языка
        self.select_language = SelectLanguage()
        layout.addLayout(self.select_language)

        self.cb_del_temp_dir = QCheckBox(
            'Удалить временные файлы после завершения работы')
        self.cb_del_temp_dir.setChecked(True)
        layout.addWidget(self.cb_del_temp_dir)

        label_tempdir = QLabel('Папка для временных файлов')
        layout.addWidget(label_tempdir)

        # поле выбра временной папки
        self.temp_dir_select = TempDirSelect()
        layout.addLayout(self.temp_dir_select)

        self.select_engine = SelectEngine()
        label_select_engine = QLabel('Голосовой движок')
        layout.addWidget(label_select_engine)
        layout.addLayout(self.select_engine)

        self.select_voice = SelectVoice()
        label_select_voice = QLabel('Голос')
        layout.addWidget(label_select_voice)
        layout.addLayout(self.select_voice)

        self.btn_run = QPushButton("Начать!")
        self.btn_run.clicked.connect(self.btn_run_click)
        layout.addWidget(self.btn_run)

        self.setLayout(layout)

        # buttons clicks

    def btn_select_film_click(self):
        file_dialog = QFileDialog()
        input_video, _ = file_dialog.getOpenFileName(self, "Выбрать фильм")
        self.field_filmname.setText(input_video)

        # autogeneration new video file name
        new_file_ext = os.path.splitext(
            os.path.basename(self.field_filmname.text()))[1]
        new_file_name = self.field_filmname.text()[:-len(new_file_ext)]
        self.field_outfilm.setText(new_file_name+"_NeuroVoiced_"+new_file_ext)

        # autofind srt file if exists
        srt_file_name = self.field_filmname.text()[:-len(new_file_ext)]+".srt"
        if ((os.path.exists(srt_file_name) == True) and (os.path.isfile(srt_file_name) == True)):
            self.field_srtname.setText(srt_file_name)

    def btn_select_tempdir_click(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.DirectoryOnly)
        dialog.setOption(QFileDialog.ShowDirsOnly, True)

        if dialog.exec_() == QFileDialog.Accepted:
            selected_folder = dialog.selectedFiles()[0]
            self.field_tempdir.setText(selected_folder)

    def btn_select_outfilm_click(self):
        file_dialog = QFileDialog()
        file_path, _ = file_dialog.getSaveFileName(
            None, 'Куда сохранить озвученный фильм ?', '', '')
        if file_path:
            self.field_outfilm.setText(file_path)

    def btn_select_srt_click(self):
        file_dialog = QFileDialog()
        input_srt, _ = file_dialog.getOpenFileName(
            self, "Выбрать SRT субтитры", '', 'SRT (*.srt);; * (*)')
        self.field_srtname.setText(input_srt)

    def btn_run_click(self):

        if self.select_engine.Selected != "":
            engine = self.select_engine.Selected
        else:
            engine = "RHV"

        if self.select_voice.Selected != "":
            default_voice = self.select_voice.Selected
        else:
            default_voice = "M"

        Start(self.temp_dir_select.Value,
              self.field_srtname.text(),
              self.field_filmname.text(),
              self.field_outfilm.text(),
              engine,
              self.field_silence.text(),
              self.speeding.Value,
              default_voice,
              self.cb_del_temp_dir.isChecked(),
              self.select_language.Value,
              self.sound_normalization.Value)
