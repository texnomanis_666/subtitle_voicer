import os
import sys
import time
import requests

from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QPalette, QColor
from gui import GUI
from utils import Start

os.system("color 0a")

if not os.path.exists("SRTtoJSON.exe"):
    print("Не найден файл парсера субтитров. Начинаю скачивание.")
    try:
        response = requests.get(
            "https://gitlab.com/texnomanis_666/srt-to-json-converter/-/raw/main/build/windows/SRTtoJSON.exe")
        with open("SRTtoJSON.exe", "wb") as file:
            file.write(response.content)
        print(f"Файл успешно скачан и сохранен как SRTtoJSON.exe")
    except Exception as e:
        os.remove("SRTtoJSON.exe")
        print(f"Произошла ошибка при скачивании файла: {e}")

os.system("title Озвучка субтитров v2.4 by TexnomaniS")


if __name__ == '__main__':
    os.system("cls")
# run gui if not comand line arguments
    if len(sys.argv) == 1:
        app = QApplication(sys.argv)

        # dark theme
        palette = QPalette()
        palette.setColor(QPalette.Window, QColor(53, 53, 53))
        palette.setColor(QPalette.WindowText, QColor(255, 255, 255))
        palette.setColor(QPalette.Base, QColor(25, 25, 25))
        palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
        palette.setColor(QPalette.ToolTipBase, QColor(255, 255, 255))
        palette.setColor(QPalette.ToolTipText, QColor(255, 255, 255))
        palette.setColor(QPalette.Text, QColor(0, 160, 0))
        palette.setColor(QPalette.Button, QColor(53, 53, 53))
        palette.setColor(QPalette.ButtonText, QColor(0, 0, 0))
        palette.setColor(QPalette.BrightText, QColor(255, 0, 0))
        palette.setColor(QPalette.Link, QColor(42, 130, 218))
        palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
        palette.setColor(QPalette.HighlightedText, QColor(0, 0, 0))
        app.setPalette(palette)

        # Создаем и отображаем главное окно
        widget = GUI()
        widget.show()

        sys.exit(app.exec_())

    # парсинг командной строки и работа в консольном режиме
    else:
        tempdir = "temp_dir"
        engine = "SILERO"
        silence = "-15"
        speeding = "1.0"
        voice = "M"

        srtname = ""
        filmname = ""
        outfilm = ""
        lang = "rus"
        delete_temp_dir = False
        normalize_sound = -10

        for i in range(0, len(sys.argv)):

            # st srt subtitles file
            if sys.argv[i].find('-st') == 0:
                srtname = sys.argv[i][3:]

            # o output video
            if sys.argv[i].find('-o') == 0:
                outfilm = sys.argv[i][2:]

            # i input video
            if sys.argv[i].find('-i') == 0:
                filmname = sys.argv[i][2:]

                # autogeneration new video file name
                if outfilm == "":
                    new_file_ext = os.path.splitext(
                        os.path.basename(filmname))[1]
                    new_file_name = filmname[:-len(new_file_ext)]
                    outfilm = new_file_name+"_NeuroVoiced_"+new_file_ext

                # autofind srt file if exists
                if srtname == "":
                    new_file_ext = os.path.splitext(
                        os.path.basename(filmname))[1]
                    srt_file_name = filmname[:-len(new_file_ext)]+".srt"
                    if ((os.path.exists(srt_file_name) == True) and (os.path.isfile(srt_file_name) == True)):
                        srtname = srt_file_name

            # e engine
            if sys.argv[i].find('-e') == 0:
                engine = sys.argv[i][2:]

            # z zatixanie (silence)
            if sys.argv[i].find('-z') == 0:
                silence = sys.argv[i][2:]

            # td temp dir
            if sys.argv[i].find('-td') == 0:
                tempdir = sys.argv[i][3:]

            # u uskorenie (speeding)
            if sys.argv[i].find('-u') == 0:
                speeding = sys.argv[i][2:]

            # dv default voice
            if sys.argv[i].find('-dv') == 0:
                voice = sys.argv[i][3:]

            # d delete temp dir
            if sys.argv[i].find('-d') == 0:
                delete_temp_dir = True

            # l language
            if sys.argv[i].find('-l') == 0:
                lang = sys.argv[i][2:]

            # n normalize sound
            if sys.argv[i].find('-n') == 0:
                normalize_sound = sys.argv[i][2:]

        print("Исходный файл:", filmname)
        print("Выходной файл:", outfilm)
        print("Файл субтитров:", srtname)
        print("")
        print("Движок:", engine)
        print("Затихание:", silence)
        print("Ускорение:", speeding)
        print("Голос:", voice)
        print("Папка для временных файлов:", tempdir)
        print("Удалить временные файлы:", delete_temp_dir)
        print("")
        time.sleep(1.5)

        Start(tempdir,
              srtname,
              filmname,
              outfilm,
              engine,
              silence,
              speeding,
              voice,
              delete_temp_dir,
              lang,
              normalize_sound)
