import pyttsx3
import torch
from progress.bar import IncrementalBar
import os
from GoogleTTS import GoogleTTS
import time

from voiceList import voiceList  # список движков и поддеживаемых ими голосов


# функция преобразующая любые вариации названия языка в вариант их списка выше
def GetLangName(lang):

    lang = lang.lower()

    if lang == "rus" or lang == "ru" or lang == "russian" or lang == "рус" or lang == "русский" or lang == "Русский":
        return "russian"

    # немецкий
    if lang == "de" or lang == "deutsch" or lang == "немецкий" or lang == "german":
        return "deutsch"

    # англиский
    if lang == "en" or lang == "english" or lang == "английский" or lang == "eng":
        return "english"

    # испанский
    if lang == "es" or lang == "espanol" or lang == "испанский" or lang == "spanish":
        return "espanol"

    # украинский
    if lang == "ua" or lang == "украинский" or lang == "ukrainian":
        return "ukrainian"

    # французский
    if lang == "fr" or lang == "французский" or lang == "french":
        return "french"

    # татарский
    if lang == "tt" or lang == "татарский" or lang == "tatar":
        return "tatar"

    # узбекский
    if lang == "uz" or lang == "узбекский" or lang == "uzbek":
        return "uzbek"


def RHVoicer(json, temp_dir, lang):
    engine = pyttsx3.init()
    voices = engine.getProperty('voices')
    bar = IncrementalBar('Озвучиваю фразу (RHVoice)', max=len(json))
    lang = GetLangName(lang)

    print('Список доступных голосв для RHVoice')
    print('==============')
    for voice in voices:
        print('Имя: %s' % voice.name)
        print('ID: %s' % voice.id)
        print('Язык(и): %s' % voice.languages)
        print('Пол: %s' % voice.gender)
        print('Возраст: %s' % voice.age)
        print('==============')

    # выбор голоса по умолчанию
        for voice in voices:
            if voice.name == 'Aleksandr':
                engine.setProperty('voice', voice.id)

    for i in range(0, len(json)):

        # выбор голоса для каждой отдельной фразы
        if "voice" in json[i]:
            if json[i]["voice"] == voiceList["rhv"][lang][json[i]["voice"]]:
                for voice in voices:
                    if voice.name == voiceList["rhv"][lang][json[i]["voice"]]:
                        engine.setProperty('voice', voice.id)

        else:  # если не указан никакой оставляем Александра
            for voice in voices:
                if voice.name == 'Aleksandr':
                    engine.setProperty('voice', voice.id)

        json[i]["WAV"] = temp_dir+'\\' + json[i]["Index"]+".wav"
        engine.save_to_file(json[i]["Text"], json[i]["WAV"])
        engine.runAndWait()
        bar.next()

    bar.finish()


def getSileroModels(lang):
    # русский
    if lang == "rus" or lang == "ru" or lang == "russian" or lang == "рус" or lang == "русский" or lang == "Русский":
        model_file = 'silero_models\\russian_tts_v3_1.pt'
        dowload_model_link = 'https://models.silero.ai/models/tts/ru/v3_1_ru.pt'

    # немецкий
    if lang == "de" or lang == "deutsch" or lang == "немецкий":
        model_file = 'silero_models\\deutsch_tts_v3.pt'
        dowload_model_link = 'https://models.silero.ai/models/tts/de/v3_de.pt'

    # англиский
    if lang == "en" or lang == "english" or lang == "английский" or lang == "eng":
        model_file = 'silero_models\\english_tts_v3.pt'
        dowload_model_link = 'https://models.silero.ai/models/tts/en/v3_en.pt'

    # англиский - индусский
    if lang == "en-indic" or lang == "english-indic" or lang == "индусский английский":
        model_file = 'silero_models\\english_indic_tts_v3.pt'
        dowload_model_link = 'https://models.silero.ai/models/tts/en/v3_en_indic.pt'

    # испанский
    if lang == "es" or lang == "espanol" or lang == "испанский":
        model_file = 'silero_models\\espanol_tts_v3.pt'
        dowload_model_link = 'https://models.silero.ai/models/tts/es/v3_es.pt'

    # украинский
    if lang == "ua" or lang == "украинский":
        model_file = 'silero_models\\ukrainskii_tts_v4.pt'
        dowload_model_link = 'https://models.silero.ai/models/tts/ua/v4_ua.pt'

    # французский
    if lang == "fr" or lang == "французский":
        model_file = 'silero_models\\fr_tts_v3.pt'
        dowload_model_link = 'https://models.silero.ai/models/tts/fr/v3_fr.pt'

    # татарский
    if lang == "tt" or lang == "татарский":
        model_file = 'silero_models\\tatar_tts_v3.pt'
        dowload_model_link = 'https://models.silero.ai/models/tts/tt/v3_tt.pt'

    # узбекский
    if lang == "uz" or lang == "узбекский":
        model_file = 'silero_models\\uzbek_tts_v4.pt'
        dowload_model_link = 'https://models.silero.ai/models/tts/uz/v4_uz.pt'

    return model_file, dowload_model_link


def SileroVoicer(json, temp_dir, lang):
    lang = GetLangName(lang)
    sample_rate = 48000
    device = torch.device('cpu')
    torch.set_num_threads(8)
    bar = IncrementalBar('Озвучиваю фразу Silero', max=len(json))

    if ((os.path.exists("silero_models") == False) and (os.path.isfile("silero_models") == False)):
        print('Создание папки для моделей Silero')
        os.mkdir("silero_models")

    model_file, dowload_model_link = getSileroModels(lang)

    if not os.path.isfile(model_file):
        print('Скачивание модели нейросети')
        torch.hub.download_url_to_file(dowload_model_link,
                                       model_file)

    print("Загрузка модели нейросети")
    model = torch.package.PackageImporter(
        model_file).load_pickle("tts_models", "model")
    model.to(device)

    for i in range(0, len(json)):
        json[i]["WAV"] = temp_dir+'\\' + json[i]["Index"]+".wav"
        text = json[i]["Text"]

        text = replace_english_to_russian(text)

        if (text != "" and text != "," and text != "." and text != "!" and text != "?" and text != ":" and text != "%" and text != "#" and text != "*" and text != "^"):
            try:
                if "voice" in json[i]:
                    speaker = voiceList["silero"][lang][json[i]["voice"]]

                else:
                    speaker = "eugene"

                audio_paths = model.save_wav(text=text,
                                             speaker=speaker,
                                             sample_rate=sample_rate)

                if ((os.path.exists(json[i]["WAV"]) == True) and (os.path.isfile(json[i]["WAV"]) == True)):
                    os.remove(json[i]["WAV"])
                os.rename("test.wav", json[i]["WAV"])
            except:
                os.system("color 0c")
                print('Ошибка озвучки ', i, json[i]["Text"])
                os.system('title Silero : Ошибка озвучки ',
                          i, json[i]["Text"], " !!!!")

        bar.next()

    bar.finish()


def GTTS(json, temp_dir, lang):

    if lang == "":
        lang = "rus"

    lang = GetLangName(lang)
    tts = GoogleTTS()
    tts.set_language_code('ru-ru')
    tts.set_ssml_gender("MALE")
    tts.set_audio_encoding('LINEAR16')

    bar = IncrementalBar('Озвучиваю фразу Google TTS', max=len(json))

    if lang == "rus" or lang == "ru" or lang == "russian" or lang == "рус" or lang == "русский" or lang == "Русский":
        tts.set_language_code('ru-ru')

    for i in range(0, len(json)):
        json[i]["WAV"] = temp_dir+'\\' + json[i]["Index"]+".wav"

        if "voice" in json[i]:
            if json[i]["voice"] == "M" or json[i]["voice"] == "M1" or json[i]["voice"] == "":
                tts.set_ssml_gender("MALE")

            if json[i]["voice"] == "F" or json[i]["voice"] == "F1":
                tts.set_ssml_gender("FEMALE")

        else:
            tts.set_ssml_gender("MALE")

        time_sleep = 1
        while os.path.exists(json[i]["WAV"]) == False:

            ret = tts.tts(json[i]["Text"], json[i]["WAV"])
            if 'audioContent' in ret:
                b64 = ret['audioContent']
            else:
                print("Файл не скачался с cерверов гугла. Ждём",
                      time_sleep, "секунд(-у -ы)")

                time.sleep(time_sleep)
                time_sleep = time_sleep+0.5

        bar.next()

    bar.finish()
