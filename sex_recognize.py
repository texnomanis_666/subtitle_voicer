
import librosa
import numpy as np
import torch

model = torch.load('VoiceSexRecognizeNN.pt')


def GetMelSpectr(wavFile):
    spectr = np.array([])
    wav, sample_rate = librosa.core.load(wavFile)
    mel = np.mean(librosa.feature.melspectrogram(y=wav, sr=44100).T, axis=0)
    spectr = np.hstack(mel)
    result = torch.from_numpy(spectr.astype(np.float32))
    return result


def Recognize(sound):

    with torch.no_grad():
        spectr = GetMelSpectr(sound)
        result = model(spectr)
        if result < 0.5:
            simbol = "F"
        else:
            simbol = "M"
        return result, simbol
